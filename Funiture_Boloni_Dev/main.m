//
//  main.m
//  Funiture_Boloni_Dev
//
//  Created by dilitech on 14-3-11.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
