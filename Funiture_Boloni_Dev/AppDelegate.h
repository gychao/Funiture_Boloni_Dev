//
//  AppDelegate.h
//  Funiture_Boloni_Dev
//
//  Created by dilitech on 14-3-11.
//  Copyright (c) 2014年 dilitech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
